Pod::Spec.new do |s|
s.name                      = "HydraSDK"
s.version                   = "0.1.0"
s.summary                   = "The Hydra iOS SDK for App Analytics."
s.homepage                  = "https://github.com/Capillary"
s.license                   = { :type => "MIT" }
s.author                    = { "Capillary" => "https://github.com/Capillary" }
#s.source                    = { :git => "https://github.com/gauravvelotio/hydra-ios-sdk.git", :tag => s.version.to_s }
#s.source                    = { :git => "git@gitlab.com:borole.gaurav/testingsdk.git", :tag => s.version.to_s }
s.source                    = { :http => 'https://gitlab.com/-/ide/project/borole.gaurav/testingsdkpodspecs/tree/main/-/HydraSDK/0.1.0/HydraSDK.xcframework.zip' }
#s.requires_arc              = true
s.module_name               = 'HydraSDK'
#s.resources                 = 'HydraSDK/*.cer'
#s.ios.resource_bundle       = {'HydraSDK' => ['HydraSDK/**/*.xcdatamodeld']}
s.ios.deployment_target     = '10.0'
#s.ios.source_files          = 'HydraSDK/**/*.{h,m}'
s.vendored_frameworks = 'HydraSDK.xcframework'
end
