Pod::Spec.new do |s|
s.name                      = "HydraSDK"
s.version                   = "0.1.1"
s.summary                   = "The Hydra iOS SDK for App Analytics."
s.homepage                  = "https://github.com/Capillary"
s.license                   = { :type => "MIT" }
s.author                    = { "Capillary" => "https://github.com/Capillary" }
#s.source                    = { :git => "https://github.com/gauravvelotio/hydra-ios-sdk.git", :tag => s.version.to_s }
s.source                    = { :git => "git@gitlab.com:borole.gaurav/testingsdk.git", :tag => s.version.to_s }
#s.source                    = { :http => 'https://gitlab.com/borole.gaurav/testingsdkpodspecs/-/blob/34be8dce064c654c02f3847f020123fb247e3417/HydraSDK/0.1.1/HydraSDK.xcframework.zip' }
#s.requires_arc              = true
#s.module_name               = 'HydraSDK'
#s.resources                 = 'HydraSDK/*.cer'
#s.ios.resource_bundle       = {'HydraSDK' => ['HydraSDK/**/*.xcdatamodeld']}
s.ios.deployment_target     = '10.0'
#s.ios.source_files          = 'HydraSDK/**/*.{h,m}'
s.vendored_frameworks = 'HydraSDK/Output/xcframeworks/HydraSDK.xcframework'
#s.vendored_frameworks = 'HydraSDK.xcframework'
end
